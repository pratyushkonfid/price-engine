import React from 'react';
import './CardComponent.scss';
class CardHeader extends React.Component {
    render() {
      const { image } = this.props;
      return (
        <header id={image} className="card-header background-image">
          {/* <h2 className="card-header--title" style={{color:'black'}}> 24.99 €</h2> */}
        </header>
      )
    }
  }
  
  class Button extends React.Component {
    render() {
      return (
        <button style={{cursor: 'pointer'}} className="button button-primary">
          <a style={{textDecoration: 'none'}} href="mailto:sales@kaizentech.io">Contact us</a>
        </button>
      )
    }
  }
  
  class CardBody extends React.Component {
    render() {
      return (
        <div className="card-body">
          <p className="date">Unlimited</p>
          
          <h2>Enterprise</h2>
          <p className="body-content">Get in touch with us and customize your transactions </p>
          
             
          <Button />
        </div>
      )
    }
  }
  
  export default class EnterpriseSubscription extends React.Component {
    render() {
      return (
        <article style={{color:'black', width:'236px'}}>
          <CardHeader image={'https://source.unsplash.com/user/erondu/600x400'}/>
          <CardBody title={'What happened in Thialand?'} text={'Kayaks crowd Three Sister Springs, where people and manatees maintain controversial coexistence'}/>
        </article>
      )
    }
  }