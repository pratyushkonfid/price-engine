import axios from 'axios';
import React from 'react';
import StripeCheckout from 'react-stripe-checkout';
import { STRIPE_LIVE_KEY } from '../env';
import './CardComponent.scss'
import { useDispatch, useSelector } from 'react-redux';

function CardHeader() {

  var style = { 
    backgroundImage: 'url("https://thumbs.dreamstime.com/b/digital-human-brain-concept-intelligence-white-background-64098709.jpg")',
  };
     
      return (
        <header className="card-header background-image">
          <h2 className="card-header--title" style={{color:'white', pointerEvents:'none'}}> 0€</h2>
        </header>
      )
    
  }
  
  function Button(){

    return (
      <button className="button button-primary">
        <i className="fa fa-chevron-right"></i> Current Plan
      </button>
    )
  }
  
  function CardBody() {
        const appAuth = useSelector(({ auth }) => auth)
    console.log(appAuth?.currentPlan);

      return (
        <div className="card-body">
          <p className="date">Current</p>
          
          <h2>Free</h2>
          <p className="body-content">25 free transactions on first registration</p>
          
          {appAuth?.currentPlan === 'free' ? 
        
          <Button />
      
          
          
          
          
          
          
          : <StripeCheckout
                    
                    stripeKey={STRIPE_LIVE_KEY}
                    name="Price Engine Subscription"
                    token={(token)=>{
                        const product = {
                            name: 'Price Engine Gold Subscription',
                            price: 0,
                          }
                        axios.post('https://price-test-engine.herokuapp.com/checkout', { token, product }).then((res)=>{
                            console.log(res)
                            // toast.success("Success! Check email for details")
                        }).catch((err)=>{
                            alert("Something went wrong", { type: "error" });
                            // toast.error(err.message);
                        })
                    }}
                    id="asdasd"
                    billingAddress
                    amount={14.99 * 100}
                    shippingAddress
                    label='Proceed to Checkout'
                    currency='EUR'
                />}
        </div>
      )
  }
  
  export default function FreeSubscription() {
    return (
      <article style={{color:'black', width:'236px'}}>
        <CardHeader />
        <CardBody title={'What happened in Thialand?'} text={'Kayaks crowd Three Sister Springs, where people and manatees maintain controversial coexistence'}/>
      </article>
    )
  }