import CloseIcon from '@mui/icons-material/Close';
import { Button, TextField } from '@mui/material';
import React from 'react';
import { textInputStyle } from '../constants';

export const TextInputFields = ({sendValuesBack, sendLoadingMeter}) => {
  const [number, setNumber] = React.useState('')
  const [length, setLength] = React.useState('');
  // let history = useHistory();
  const [width, setWidth] = React.useState('');
  const [height, setHeight] = React.useState('');
  const [weight, setWeight] = React.useState('');
  const [disabled, setDisabled] = React.useState(false);

  return (
    <div className="shipmentDetails-shipment-fields-others-inputfields">
    <div>   <TextField placeholder='Number' InputLabelProps={{ shrink: true }} sx={textInputStyle} value={number} onChange={(e) => { setNumber(e.target.value) }}  id="outlined-basic" variant="outlined" type="number" /></div>
    <div>   <TextField InputLabelProps={{ shrink: true }} sx={textInputStyle} id="outlined-basic" placeholder="Type of Packaging" variant="outlined" type="text" /></div>
    <div>   <TextField sx={textInputStyle} value={length} onChange={(e) => { setLength(e.target.value) }} id="outlined-basic" placeholder="Length" variant="outlined" type="number" /></div>
    <div>   <TextField sx={textInputStyle} value={width} onChange={(e) => { setWidth(e.target.value) }} id="outlined-basic" placeholder="Width" variant="outlined" type="number" /></div>
    <div>   <TextField sx={textInputStyle} value={height} onChange={(e) => { setHeight(e.target.value) }} id="outlined-basic" placeholder="Height" variant="outlined" type="number" /></div>
    <div>   <TextField sx={textInputStyle} value={weight} onChange={(e) => { setWeight(e.target.value);setDisabled(false)  }} id="outlined-basic" placeholder="Unit weight" variant="outlined" type="number" />
    <Button disabled={disabled} onClick={()=>{sendValuesBack({
      number,
      length,
      width,
      height,
      weight: parseFloat(number) * parseFloat(weight)
    });sendLoadingMeter((parseFloat(number) * parseFloat(width) * parseFloat(length)) / 24000);console.log('individual loading meter', parseFloat(number) * parseFloat(width) * parseFloat(length) / 24000); setDisabled(true)}}>Add</Button></div>

  </div>
  )
}

export default TextInputFields