import { Button } from '@mui/material'
import React from 'react'
import intl from '../translations'

export const ShipmentOptions = ({selectedOption, setSelectedOption}) => {
  return (
    <div className="shipmentDetails-shipment-details-options">
    <div className="shipmentDetails-shipment-details-options-container">
      <Button className={selectedOption === 'Euro Pallet' ? 'selected-class' : ''} onClick={() => { setSelectedOption('Euro Pallet') }}>{intl.formatMessage({
        id: 'app.shipment.euroPallet',
      })}</Button>
    </div>
    <div className="shipmentDetails-shipment-details-options-container">
      <Button className={selectedOption === 'Pallet' ? 'selected-class' : ''} onClick={() => { setSelectedOption('Pallet') }}>
        {intl.formatMessage({
          id: 'app.shipment.pallet',
        })}
      </Button>
    </div>
    <div className="shipmentDetails-shipment-details-options-container">
      <Button className={selectedOption === 'Box' ? 'selected-class' : ''} onClick={() => { setSelectedOption('Box') }}>
        {intl.formatMessage({
          id: 'app.shipment.box',
        })}
      </Button>
    </div>
    <div className="shipmentDetails-shipment-details-options-container">
      <Button className={selectedOption === 'Crate' ? 'selected-class' : ''} onClick={() => { setSelectedOption('Crate') }}>
        {intl.formatMessage({
          id: 'app.shipment.crate',
        })}
      </Button>
    </div>
    <div className="shipmentDetails-shipment-details-options-container">
      <Button className={selectedOption === 'Bundle' ? 'selected-class' : ''} onClick={() => { setSelectedOption('Bundle') }}>
        {intl.formatMessage({
          id: 'app.shipment.bundle',
        })}
      </Button>
    </div>
    <div className="shipmentDetails-shipment-details-options-container">
      <Button className={selectedOption === 'Drum' ? 'selected-class' : ''} onClick={() => { setSelectedOption('Drum') }}>
        {intl.formatMessage({
          id: 'app.shipment.drum',
        })}
      </Button>
    </div>
    <div className="shipmentDetails-shipment-details-options-container">
      <Button className={selectedOption === 'Roll' ? 'selected-class' : ''} onClick={() => { setSelectedOption('Roll') }}>
        {intl.formatMessage({
          id: 'app.shipment.roll',
        })}
      </Button>
    </div>
    <div className="shipmentDetails-shipment-details-options-container">
      <Button className={selectedOption === 'Bale' ? 'selected-class' : ''} onClick={() => { setSelectedOption('Bale') }}>
        {intl.formatMessage({
          id: 'app.shipment.bale',
        })}
      </Button>
    </div>
  </div>
  )
}

export default ShipmentOptions