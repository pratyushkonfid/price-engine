import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import { useHistory } from 'react-router-dom';

import image from '../assets/images/kaizen.png'
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
export default function Navbar() {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };
    let history = useHistory();
    


    const [anchorEl2, setAnchorEl2] = React.useState(null);
    const open2 = Boolean(anchorEl2);
    const handleClick2 = (event) => {
        setAnchorEl2(event.currentTarget);
    };
    const handleClose2 = () => {
        setAnchorEl2(null);
    };

    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar style={{background: '#1976d285'}} position="static">
                <Toolbar>
                    <IconButton
                    onClick={()=>{history.push('/')}}
                        size="small"
                        edge="start"
                        color="inherit"
                        aria-label="menu"
                        sx={{ mr: 2 }}
                    >
                        <img style={{ width: '55px', marginLeft: '20px' }} src={image} />
                    </IconButton>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        {/* News */}
                    </Typography>
                    <Button color="inherit">

                        <div>
                            <Button
                                style={{color: 'white'}}
                                id="basic-button"
                                aria-controls={open ? 'basic-menu' : undefined}
                                aria-haspopup="true"
                                aria-expanded={open ? 'true' : undefined}
                                onClick={handleClick}
                            >
                                Accounts
                            </Button>
                            <Menu
                                id="basic-menu"
                                anchorEl={anchorEl}
                                open={open}
                                onClose={handleClose}
                                MenuListProps={{
                                    'aria-labelledby': 'basic-button',
                                }}
                            >
                                <MenuItem onClick={()=>{history.push('/signup')}}>Sign Up</MenuItem>
                                <MenuItem onClick={()=>{history.push('/login')}}>Login</MenuItem>
                                <MenuItem onClick={()=>{history.push('/forgot_password')}}>Forgot Password</MenuItem>
                            </Menu>
                        </div>

                    </Button>
                    <Button color="inherit">

                        {/* <div>
                            <Button
                                style={{color: 'white'}}
                                id="basic-button"
                                aria-controls={open2 ? 'basic-menu' : undefined}
                                aria-haspopup="true"
                                aria-expanded={open2 ? 'true' : undefined}
                                onClick={handleClick2}
                            >
                                Pricing
                            </Button>
                            <Menu
                                id="basic-menu"
                                anchorEl={anchorEl2}
                                open={open2}
                                onClose={handleClose2}
                                MenuListProps={{
                                    'aria-labelledby': 'basic-button',
                                }}
                            >
                                <MenuItem onClick={()=>{history.push('/manage_subscriptions')}}>Manage Subscriptions</MenuItem>
                            </Menu>
                        </div> */}

                    </Button>
                    <Button onClick={()=>{history.push('/contact')}} color="inherit">Contact</Button>
                </Toolbar>
            </AppBar>
        </Box>
    );
}
