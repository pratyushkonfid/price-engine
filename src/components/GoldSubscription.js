import axios from 'axios';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import StripeCheckout from 'react-stripe-checkout';
import { STRIPE_LIVE_KEY } from '../env';
import { setCurrentPlan } from '../redux/actions/actions';
import './CardComponent.scss'
import ComingSoonModal from './ComingSoonModal';
class CardHeader extends React.Component {
    render() {
      const { image } = this.props;
      return (
        <header id={image} className="card-header background-image">
          <h2 className="card-header--title" style={{color:'white', pointerEvents:'none'}}> 14.99 €</h2>
        </header>
      )
    }
  }
  
  class Button extends React.Component {
    render() {
      return (
        <button className="button button-primary">
          <i className="fa fa-chevron-right"></i> Current Plan
        </button>
      )
    }
  }
  
  function CardBody(){
    const appAuth = useSelector(({ auth }) => auth)
    console.log(appAuth?.currentPlan);
    const dispatch = useDispatch()
    const [show, setShow] = React.useState(false);

      return (
        <div className="card-body">
          {show && <ComingSoonModal show={show} setShow={setShow} />}
          <p className="date">Upgrade</p>
          
          <h2>Gold</h2>
          <p className="body-content">100 transactions after successful payment</p>
          
          <div className='checkoudt'>
                {appAuth?.currentPlan !== 'gold' ? 
                // PAYMENT GATEWAY TO BE PAUSED FOR BETA VERSION
                // <StripeCheckout
                    
                //     stripeKey={STRIPE_LIVE_KEY}
                //     name="Price Engine Subscription"
                //     token={(token)=>{
                //         const product = {
                //             name: 'Price Engine Gold Subscription',
                //             price: 1499,
                //           }
                //         axios.post('https://price-test-engine.herokuapp.com/checkout', { token, product }).then((res)=>{
                //             console.log(res)
                //             // console.log(res.data.access_token);
                //             // make another api call to update the details in the backend
                //             // toast.success("Success! Check email for details")
                //             console.log(res);
                //             axios.post('https://kaizentestapi.herokuapp.com/subscription/', {
                //               "bundle_name": "gold",
                //               "transaction_id": res.data.transaction_id.id
                //             }, {
                //               headers: {
                //                   'Authorization': `Bearer ${localStorage.access_token}`
                //               }
                //             }).then((res)=>{
                //               console.log(res)
                //               dispatch(setCurrentPlan('gold'));
                //             }).catch((err)=>{
                //               console.log(err);
                //             })
                //         }).catch((err)=>{
                //             // alert("Something went wrong", { type: "error" });
                //             console.log(err);
                //             // toast.error(err.message);
                //         })
                //     }}
                //     id="asdasd"
                //     billingAddress
                //     amount={14.99 * 100}
                //     shippingAddress
                //     label='Proceed to Checkout'
                //     currency='EUR'
                // />
                <button onClick={()=>{
                  //beta version, show popup coming soon
                  setShow(true);
                }} className="button button-primary coming-soon">Proceed to Checkout</button>
                
                :<Button/>}
            </div>
        </div>
      )
  }
  
  export default class GoldSubscription extends React.Component {
    render() {
      return (
        <article style={{color:'black', width:'236px'}}>
          <CardHeader image={'https://source.unsplash.com/user/erondu/600x400'}/>
          <CardBody title={'What happened in Thialand?'} text={'Kayaks crowd Three Sister Springs, where people and manatees maintain controversial coexistence'}/>
        </article>
      )
    }
  }