// eslint-disable-next-line import/no-anonymous-default-export
export default {
    'app.signup.label': 'Sign Up',
    'app.login.label': 'Log In',
    'app.login.forgotPassword': 'Forgot Password',

    'app.dashboard.pickup': 'Pick Up',
    'app.dashboard.dropoff': 'Drop Off',
    'app.dashboard.next': 'Next',

    'app.shipment.enterShipmentDetails': 'Enter Shipment Details',
    'app.shipment.typeOfPackaging': 'Type of packaging',
    'app.shipment.euroPallet': 'Euro Pallet',
    'app.shipment.pallet': 'Pallet',
    'app.shipment.box': 'Box',
    'app.shipment.crate': 'Crate',
    'app.shipment.bundle': 'Bundle',
    'app.shipment.drum': 'Drum',
    'app.shipment.roll': 'Roll',
    'app.shipment.bale': 'Bale',
    'app.shipment.next': 'Next',
    'app.shipment.numberOfPallets': 'Number of Pallets',
    'app.shipment.totalWeight': 'Total Weight',

    'app.manageSubscriptions.logo': 'Logo',
    'app.manageSubscriptions.free': 'Free',
    'app.manageSubscriptions.current': 'Current',
    'app.manageSubscriptions.freeSubscriptions': '25 free transactions on first registration',
    'app.manageSubscriptions.gold': 'Gold',
    'app.manageSubscriptions.goldTransactions': '100 transactions',
    'app.manageSubscriptions.diamond': 'Diamond',
    'app.manageSubscriptions.diamondTransactions': '100 transactions',
    'app.manageSubscriptions.recommended': 'Recommended',
    'app.manageSubscriptions.enterprise': 'Enterprise',
    'app.manageSubscriptions.unlimited': 'Unlimited',
    'app.manageSubscriptions.contact': 'Contact (sales@kaizentech.io)',

    'app.welcomePage.about': 'About',
    'app.welcomePage.contact': 'Contact',
    'app.welcomePage.pricing': 'Pricing',
    'app.welcomePage.createAccount': 'Create Account',
    'app.welcomePage.manageSubscriptions.label': 'Manage Subscriptions',
    'app.welcomePage.resetPassword': 'Reset Password',

    'app.payment.expiredMessage': 'Your subscription has expired! Please upgrade to continue using our services',

    'app.calculatePrice.recommendedPrice': 'Recommended Price',
    'app.calculatePrice.recommendedRange': 'Recommended Range',
    'app.calculatePrice.fullTrucksNeeded': 'Full Trucks needed',
    'app.calculatePrice.lessThanATruck': 'Less than a full truck needed',
    'app.calculatePrice.distance': 'Distance'


    

  };
  