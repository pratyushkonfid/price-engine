import { createIntl, createIntlCache } from 'react-intl';
import en from './en';
const cache = createIntlCache();
const englishMessages = {
  ...en,
};

const getLanguage = () => {
    return {
        locale: 'EN',
        messages: englishMessages,
      }
}



const intl = createIntl(getLanguage(), cache);
export default intl;
