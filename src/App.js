import React from "react";

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Dashboard from "./pages/Dashboard";
import Login from "./pages/Login";
import SignUp from "./pages/SignUp";
import './App.css'
import ShipmentDetails from "./pages/ShipmentDetails";
import CalculatePrice from "./pages/CalculatePrice";
import Payment from "./pages/Payment";
import WelcomePage from "./pages/WelcomePage";
import ManageSubscriptions from "./pages/ManageSubscriptions";
import ResetPassword from "./pages/ResetPassword";
import UpdatePassword from "./pages/UpdatePassword";
import ChargebeePage from "./pages/ChargebeePage";
import { Card, Main } from "./components/CardComponent";
import ContactPage from "./pages/ContactPage";
import Navbar from "./components/Navbar";
import ForgotPassword from "./pages/ForgotPassword";
import ResetUpdatePassword from "./pages/ResetUpdatePassword";



function App() {

  
  return (
    <Router>
    <div>
      <Switch>
        <Route exact path="/">
        <WelcomePage />
        </Route>
        <Route exact path="/signup">
         <SignUp />
        </Route>
        <Route exact path="/dashboard">
          <Dashboard />
        </Route>
        <Route exact path="/payment">
          <Payment />
        </Route>
        <Route exact path="/login">
          <Login/>
        </Route>
        <Route exact path="/update_password">
          <UpdatePassword/>
        </Route>
        <Route exact path="/reset_password">
          <ResetPassword/>
        </Route>
        <Route exact path="/forgot_password">
          <ForgotPassword/>
        </Route>
        <Route exact path="/manage_subscriptions">
          <ManageSubscriptions />
        </Route>
        <Route exact path="/chargebee">
          <ChargebeePage />
        </Route>
        <Route exact path="/cards">
        <Card />
        </Route>
        <Route exact path="/forgot_password/:id">
        <ResetUpdatePassword />
        </Route>
        <Route exact path="/contact">
        <ContactPage />
        </Route>
        <Route exact path="/dashboard/:id/" children={<ShipmentDetails />}/>   
        <Route path="/dashboard/:id/price" children={<CalculatePrice />}/>   
        <Route path="/navbar" children={<Navbar/>}/>   
      </Switch>
    </div>
  </Router>
  );
}

export default App;
