import axios from 'axios';
import { BACKEND_URL, THE_ZIP_CODES_URL } from '../env';
export const login=(email,password)=>{
    return new Promise((resolve,reject)=>{
        let axiosConfig = {
            headers: {
              'Content-Type': 'application/json;charset=UTF-8',
              "Access-Control-Allow-Origin": "*",
            }
          };
          axios.post(`https://kaizentestapi.herokuapp.com/login`, {
            email,
            password
          }, axiosConfig).then((res) => {
            localStorage.setItem('access_token', res.data.access_token);
            resolve(res)
          }).catch((err) => {
            reject(err)
      
          })
    })
}


export const signup=(email,password, firstName, lastName)=>{
    return new Promise((resolve,reject)=>{
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
        };
        axios.post(`https://kaizentestapi.herokuapp.com/register`, {
            email,
            password,
            first_name : firstName, 
            last_name: lastName
        }, axiosConfig).then((res) => {
            resolve(res)
            // history.push('/login')
        }).catch((err) => {
            reject(err.response.data.detail);
        })
    })
}

export const checkout = ( token, product) => {
    return new Promise((resolve, reject) => {
        axios.post(
            "https://price-test-engine.herokuapp.com/checkout",
            { token, product }
        ).then((res)=>{
            resolve(res)
        }).catch((err)=>{
            reject(err)
        })
    })
}

export const calculateDistance = (pickUpPincode, dropOffPincode) => {
    return new Promise((resolve, reject) => {
        axios.get(`${THE_ZIP_CODES_URL}/api/v1/distance?fromZipCode=${pickUpPincode}&toZipCode=${dropOffPincode}&unit=MI&countryCode=DE&apiKey=cbbdd1f35b3d7bd690cee98527aee610`).then((res)=>{
            resolve(res)
        }).catch((err)=>{
            reject(err);
        })
    })
}

export const predictPrice = (data) => {
    return new Promise((resolve, reject) => {
        axios.post(`https://kaizentestapi.herokuapp.com/pengine/`,
            data, {
                headers: {
                    'Authorization': `Bearer ${localStorage.access_token}`
                }
        }).then((res)=>{
            resolve(res)
        }).catch((err)=>{
            reject(err);
        })
    })
}

export const getLocation = (zipcode) => {
    return new Promise((resolve, reject) => {
        axios.get(`${THE_ZIP_CODES_URL}/api/v1/search?zipCode=${zipcode}&countryCode=&apiKey=ff577b079e3556a60846e2677128e2f1`).then((res)=>{
            resolve(res);
        }).catch((err)=>{
            reject(err);
        })
    })
}