import React from 'react';
import { useDispatch } from 'react-redux';
import StripeCheckout from "react-stripe-checkout";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { STRIPE_LIVE_KEY } from '../env';
import { checkoutPayment } from '../redux/actions/actions';
import intl from '../translations';
import '../styles/Payment.css'
import { useHistory } from "react-router-dom";

import { Button } from '@mui/material';
function Payment() {
  const dispatch = useDispatch()
  const productPrice = 150000
  let history = useHistory();
  toast.configure();
  const product = {
    name: 'Price Engine Subscription',
    price: productPrice
  }
  React.useEffect(() => {
    toast.error('Your subscription has expired')
  })
  async function handleToken(token, addresses) {
    const response = await dispatch(checkoutPayment(token, product))
    const { status } = response.data;
    if (status === "success") {
      toast.success("Success! Check email for details")
    } else {
      alert("Something went wrong", { type: "error" });
      toast.error("Something went wrong");
    }
  }
  return (
    <div>
      {/* <React.Fragment>    
        <h3>{intl.formatMessage({
        id: 'app.payment.expiredMessage',
      })}</h3>
      </React.Fragment>
      <StripeCheckout
       stripeKey={STRIPE_LIVE_KEY}
        name="Price Engine Subscription"
        token={handleToken}
        billingAddress
        amount={product.price}
        shippingAddress
        currency='EUR'
      /> */}
      <div className="payment-center-div">
      <h3>{intl.formatMessage({
        id: 'app.payment.expiredMessage',
      })}</h3>
      <Button onClick={()=>{history.push('/manage_subscriptions')}} className="pay-button">Pay with Card</Button>
      </div>
    </div>
  )
}

export default Payment