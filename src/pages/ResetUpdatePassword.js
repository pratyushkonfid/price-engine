import LockResetIcon from '@mui/icons-material/LockReset';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import * as React from 'react';
import axios from 'axios';
import { AppBar, Menu, MenuItem, Toolbar } from '@mui/material';
import { useDispatch } from 'react-redux';
import { useHistory } from "react-router-dom";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Navbar from '../components/Navbar';

const theme = createTheme();
export default function ResetUpdatePassword() {
    const token = window.location.pathname.split('/')[2];
  const [password, setPassword] = React.useState('');
  const [confirmPassword, setConfirmPassword] = React.useState('');
  let history = useHistory();
  const [errorMessage, setErrorMessage] = React.useState('')
  const dispatch = useDispatch()
  toast.configure();

  const updatePasswordHandler = (e) => {
      e.preventDefault()
      if(password === confirmPassword){
        axios.put('https://kaizentestapi.herokuapp.com/user/updatepwd', {
          new_password: password
        }, {
          headers: {
              'Authorization': `Bearer ${token}`
          }
        }).then((res)=>{
          console.log(res);
          toast.success('Password updated! Please login with the new password')
          history.push('/login')
        }).catch((err)=>{
          console.log(err)
          setErrorMessage(err.response.data.message)
          toast.error(err.response.data.message);
      })
      }
      else {
        toast.error("Passwords dont match")
      }

  }
  return (
    <ThemeProvider theme={theme}>
      <Navbar />
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockResetIcon />
          </Avatar>
          <Typography component="h1" style={{color:'white'}} variant="h5">
            Enter a new password
          </Typography>
          <Box component="form" noValidate sx={{ mt: 1 }}>
          <TextField
              margin="normal"
              value={password}
              sx={{ input: { color: 'black', background: 'white' } }}
              onChange={(e) => { setPassword(e.target.value); setErrorMessage('') }}
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
             <TextField
              margin="normal"
              value={confirmPassword}
              onChange={(e) => { setConfirmPassword(e.target.value); setErrorMessage('') }}
              required
              fullWidth
              sx={{ input: { color: 'black', background: 'white' } }}
              name="password"
              label="Confirm Password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              onClick={updatePasswordHandler}
              sx={{ mt: 3, mb: 2 }}
            >
             Update Password
            </Button>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
}