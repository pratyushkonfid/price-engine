import React from 'react'
import '../styles/WelcomePage.css';
import { useHistory } from 'react-router-dom';
import intl from '../translations';
import image from '../assets/images/kaizen.png'
import Navbar from '../components/Navbar';
export const WelcomePage = () => {
    let history = useHistory();

    return (
        <div className="welcome-page">
            <Navbar />
           
            <div className="welcome-page-title">
                <h1>KAIZen</h1>
                <h2>Freight Pricing Engine</h2>
            </div>
        </div>
    )
}

export default WelcomePage