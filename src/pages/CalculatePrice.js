import { AppBar, Button, Menu, MenuItem, Toolbar } from '@mui/material';
import React from 'react';
import Loader from 'react-loader';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { predictPrice } from '../api';
import { calculatePincodesDistance } from '../redux/actions/actions';
import '../styles/CalculatePrice.css';
import intl from '../translations';
import { Typography } from '@mui/material';
import { Paper } from '@mui/material';
import { Box } from '@mui/system';
function CalculatePrice() {
    const truckWidth = 24000;
    const [recommendedPrice, setRecommendedPrice] = React.useState('')
    const appAuth = useSelector(({ auth }) => auth)
    const [calculatedDistance, setCalculatedDistance] = React.useState('')
    const [lowerBand, setLowerBand] = React.useState('');
    const [numberOfFTL, setNumberOfFTL] = React.useState('')
    const [numberOfLTL, setNumberOfLTL] = React.useState('');
    const [upperBand, setUpperBand] = React.useState('');
    const [errorMessage, setErrorMessage] = React.useState('');
    const dispatch = useDispatch()
    const [showData, setShowData] = React.useState(false);
    let history = useHistory();
    const calculatepriceengine = () => {
        dispatch(calculatePincodesDistance(appAuth.pickupPincode, appAuth.dropoffPincode)).then((res) => {
            setCalculatedDistance(res.data.result.distance);
            // const ldm = parseFloat(appAuth.shipment_details.number) * parseFloat(appAuth.shipment_details.width) * parseFloat(appAuth.shipment_details.length) / truckWidth;
            predictPrice({
                "loading_type": "ftl",
                "distance": res.data.result.distance,
                "weight": appAuth.shipment_details.weight,
                "ldm": appAuth.loadingMeter
            }).then((res) => {
                setRecommendedPrice(res.data.price);
                setUpperBand(res.data.upper_band);
                setNumberOfLTL(res.data.num_ltl)
                setNumberOfFTL(res.data.num_ftl)
                setLowerBand(res.data.lower_band)
                setShowData(true);
            }).catch((err) => {
                setErrorMessage(err.response.data.message);
                if (err.response.data.message.includes('no active plans')) {
                    history.push('/payment')
                }
            })
        })
    }

    React.useEffect(() => {
        calculatepriceengine();
    })
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };
    return (
        <div className="calculateprice">

            <AppBar style={{ background: '#1976d285' }} position="static">
            <Toolbar style={{display:'flex', justifyContent:'flex-end'}}>


                    <div >
                        <Button
                            style={{ color: 'white' }}
                            id="basic-button"
                            aria-controls={open ? 'basic-menu' : undefined}
                            aria-haspopup="true"
                            aria-expanded={open ? 'true' : undefined}
                            onClick={handleClick}
                        >
                            Accounts
                        </Button>
                        <Menu
                            id="basic-menu"
                            anchorEl={anchorEl}
                            open={open}
                            onClose={handleClose}
                            MenuListProps={{
                                'aria-labelledby': 'basic-button',
                            }}
                        >
                            <MenuItem onClick={() => { history.push('/dashboard') }}>Home</MenuItem>
                            <MenuItem onClick={() => { history.push('/reset_password') }}>Reset Password</MenuItem>
                            <MenuItem onClick={() => { history.push('/manage_subscriptions') }}>Plans</MenuItem>
                            <MenuItem onClick={() => { history.push('/') }}>Logout</MenuItem>
                        </Menu>
                    </div>
                </Toolbar>
            </AppBar>
            {errorMessage.length > 0 ? <div>{errorMessage}</div> : <>
                <Loader loaded={showData}>
                    <div className="price-div">
                        {/* <div className="calculateprice-price"> */}
                        <Paper className="calculateprice-price">
                            <Typography style={{display:'flex'}} variant="h6">{intl.formatMessage({
                                id: 'app.calculatePrice.recommendedPrice',
                            })}: 
                              <Typography marginLeft={2} variant="h5">
                              €{recommendedPrice}
                              </Typography>
                             
                             
                            </Typography>
                        </Paper>



                        {/* </div> */}
                        <Paper className="calculateprice-range">
                            <Typography style={{display:'flex'}}  variant="h6">{intl.formatMessage({
                                id: 'app.calculatePrice.recommendedRange',
                            })}:  <Typography marginLeft={2} variant="h5">€{upperBand} - €{recommendedPrice} - €{lowerBand}</Typography></Typography>

                        </Paper>
                        <Typography marginTop="40px" color="white" variant="h5">
                                Additional Information
                            </Typography>
                        <Paper className="calculateprice-range">
                            <Typography style={{display:'flex'}}   variant="h6">
                                {intl.formatMessage({
                                    id: 'app.calculatePrice.fullTrucksNeeded',
                                })}{' '}(FTL): <Typography marginLeft={2} variant="h5">{numberOfFTL}</Typography>
                            </Typography>

                        </Paper>
                  

                        <Paper className="calculateprice-range">
                      
                            <Typography style={{display:'flex'}}   variant="h6">
                                {intl.formatMessage({
                                    id: 'app.calculatePrice.lessThanATruck',
                                })}{' '}(LTL): <Typography marginLeft={2} variant="h5">{numberOfLTL}</Typography>
                            </Typography>
                        </Paper>
                        <Paper className="calculateprice-range">
                            <Typography style={{display:'flex'}}   variant="h6">
                                {intl.formatMessage({
                                    id: 'app.calculatePrice.distance',
                                })}: <Typography marginLeft={2} variant="h5">{parseFloat(calculatedDistance).toFixed(2)} {' '}km</Typography>
                            </Typography>
                        </Paper>
                    </div>
                </Loader>
            </>}
        </div>
    )
}

export default CalculatePrice