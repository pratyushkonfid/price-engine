import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import FormControlLabel from '@mui/material/FormControlLabel';
import { textInputStyle } from '../constants';
import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import * as React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from "react-router-dom";
import Navbar from '../components/Navbar';
import { loginUser, setCurrentPlan, setCurrentUser } from '../redux/actions/actions';
import axios from 'axios';
import intl from '../translations';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
const theme = createTheme();

export default function Login() {
  const [email, setEmail] = React.useState('')
  const [password, setPassword] = React.useState('');
  let history = useHistory();
  const [errorMessage, setErrorMessage] = React.useState('')
  const dispatch = useDispatch()
  toast.configure();

  const handleSubmit = (event) => {
    event.preventDefault();
    dispatch(loginUser(email, password)).then((res) => {
      event.preventDefault();
      console.log('token', res.data.access_token);
      let access_token = res.data.access_token
      localStorage.setItem('access_token', res.data.access_token);
      //fetch users current plan and update redux
      axios.get('https://kaizentestapi.herokuapp.com/user/currentplan', {
        headers: {
            'Authorization': `Bearer ${res.data.access_token}`
        }
    }).then((res)=>{
        console.log(res);
        dispatch(setCurrentUser(email));
        dispatch(setCurrentPlan(res?.data?.using_plan.name));
        history.push('/dashboard')
      }).catch((err)=>{
        console.log('x', err.response.data)
        // subscription not found for this user
        console.log(err.response.data.detail);
        if(err.response.data.detail.includes('subscription not found')){
          // alert('subscription not found')
          //store to redux with free plan
          dispatch(setCurrentUser(email));
          dispatch(setCurrentPlan('free'));
          // activate free bundle for the user
          axios.post('https://kaizentestapi.herokuapp.com/subscription/', {
            "bundle_name": "free",
          },{
            headers: {
                'Authorization': `Bearer ${access_token}`
            }
        }).then((res)=>{
            history.push('/dashboard')
          })
        }
      })

      // history.push('/dashboard')
    }).catch((err) => {
      console.log('x', err)
      toast.error("Please confirm your email");
      setErrorMessage(err.response.data.message);
    })
  };

  return (
    <ThemeProvider theme={theme}>
      <Navbar />
      <Container component="main" maxWidth="xs">

        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            {intl.formatMessage({
              id: 'app.login.label',
            })}
          </Typography>
          <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              value={email}
              onChange={(e) => { setEmail(e.target.value); setErrorMessage('') }}
              required
              sx={textInputStyle}
              fullWidth
              id="email"
              placeholder="Email Address"
              name="email"
              autoComplete="email"
              InputLabelProps={{ shrink: true }} 
              autoFocus
            />
            <TextField
              margin="normal"
              value={password}
              sx={textInputStyle}
              onChange={(e) => { setPassword(e.target.value); setErrorMessage('') }}
              required
              fullWidth
              name="password"
              InputLabelProps={{ shrink: true }} 
              placeholder="Password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              {intl.formatMessage({
                id: 'app.login.label',
              })}
            </Button>
            <Grid container>
              <Grid item xs>
                <Link onClick={()=>{history.push('/forgot_password')}} variant="body2" style={{ color: 'white' }}>
                  {intl.formatMessage({
                    id: 'app.login.forgotPassword',
                  })}
                </Link>
              </Grid>
              <Grid item>
                <Link href="/signup" variant="body2" style={{ color: 'white' }}>
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
}