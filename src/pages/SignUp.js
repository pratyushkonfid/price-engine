import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import { Grid } from '@mui/material';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Link from '@mui/material/Link';

import Checkbox from '@mui/material/Checkbox';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import FormControlLabel from '@mui/material/FormControlLabel';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import * as React from 'react';
import { useDispatch } from 'react-redux';
// import { Link } from 'react-router-dom';
import { useHistory } from "react-router-dom";
import axios from 'axios';
import { signupUser } from '../redux/actions/actions';
import intl from '../translations';
import Navbar from '../components/Navbar';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { textInputStyle } from '../constants';
import { signup } from '../api';
const theme = createTheme();

export default function Login() {
    const [email, setEmail] = React.useState('')
    let history = useHistory();
    const [password, setPassword] = React.useState('');
    const [firstName, setFirstName] = React.useState('');
    const [lastName, setLastName] = React.useState('');
    toast.configure()
    const [confirmPassword, setConfirmPassword] = React.useState('')
    const dispatch = useDispatch()
    const [errorMessage, setErrorMessage] = React.useState('')
    const [emailConfirmation, setEmailConfirmation] = React.useState(false);
    const handleSubmit = (event) => {
        if (confirmPassword === password) {
            event.preventDefault();
            signup(email, password, firstName, lastName).then((res) => {
                console.log(res);
                event.preventDefault();
                axios.post('https://kaizentestapi.herokuapp.com/verify/', {
                    "email": email,
                    "reason": "email_confirmation"
                }).then((res) => {
                    console.log(res)
                    setEmailConfirmation(true);
                })
            }).catch((err) => {
                toast.error(err);
                setErrorMessage(err.response.data.message);
            })
        }
        else {
            event.preventDefault();
            toast.error("Passwords do not match")
        }

    };

    return (
        <ThemeProvider theme={theme}>
            <Navbar />
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography color="white" component="h1" variant="h5">
                        {intl.formatMessage({
                            id: 'app.signup.label',
                        })}
                    </Typography>
                    {!emailConfirmation ? <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
                        <div style={{ display: 'flex', }}> <TextField
                            style={{ width: '50%', marginRight: '10px' }}
                            margin="normal"
                            sx={textInputStyle}
                            InputLabelProps={{ shrink: true }}

                            onChange={(e) => { setFirstName(e.target.value); setErrorMessage('') }}
                            required
                            value={firstName}
                            fullWidth
                            placeholder="First Name"
                        />
                            <TextField
                                InputLabelProps={{ shrink: true }}

                                style={{ width: '50%' }}
                                margin="normal"
                                sx={textInputStyle}
                                onChange={(e) => { setLastName(e.target.value); setErrorMessage('') }}
                                required
                                value={lastName}
                                fullWidth
                                placeholder="Last Name"
                            /></div>

                        <TextField
                            margin="normal"
                            sx={textInputStyle}
                            value={email}
                            InputLabelProps={{ shrink: true }}

                            onChange={(e) => { setEmail(e.target.value); setErrorMessage('') }}
                            required
                            fullWidth
                            id="email"
                            placeholder="Email Address"
                            name="email"
                            autoComplete="email"
                            autoFocus
                        />
                        <TextField
                            margin="normal"
                            value={password}
                            sx={textInputStyle}
                            onChange={(e) => { setPassword(e.target.value); setErrorMessage('') }}
                            required
                            InputLabelProps={{ shrink: true }}

                            fullWidth
                            name="password"
                            placeholder="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                        />
                        <TextField
                            margin="normal"
                            sx={textInputStyle}
                            value={confirmPassword}
                            onChange={(e) => { setConfirmPassword(e.target.value); setErrorMessage('') }}
                            required
                            InputLabelProps={{ shrink: true }}

                            fullWidth
                            name="password"
                            placeholder="Confirm Password"
                            type="password"
                            id="confirmPassword"
                            autoComplete="current-password"
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            sx={{ mt: 3, mb: 2 }}
                        >
                            {intl.formatMessage({
                                id: 'app.signup.label',
                            })}
                        </Button>
                        <Grid item>
                            <Link href="/login" variant="body2" style={{ color: 'white' }}>
                                Don't have an account? Login
                            </Link>
                        </Grid>
                    </Box> :

                        <><Box>    <Typography component="h2" style={{ color: 'white', marginTop: '20px' }} variant="h6">
                            Check your email for confirmation.
                        </Typography></Box></>}
                </Box>
            </Container>
        </ThemeProvider>
    );
}