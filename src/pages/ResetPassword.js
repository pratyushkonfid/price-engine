import LockResetIcon from '@mui/icons-material/LockReset';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import * as React from 'react';
import axios from 'axios';
import { AppBar, Menu, MenuItem, Toolbar } from '@mui/material';
import { useDispatch } from 'react-redux';
import { useHistory } from "react-router-dom";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Navbar from '../components/Navbar';
import { textInputStyle } from '../constants';

const theme = createTheme();
export default function ForgotPassword() {
  const [password, setPassword] = React.useState('');
  let history = useHistory();
  const [errorMessage, setErrorMessage] = React.useState('')
  const dispatch = useDispatch()
  toast.configure();

  const updatePasswordHandler = (e) => {
      e.preventDefault()
      axios.put('https://kaizentestapi.herokuapp.com/user/updatepwd', {
        new_password: password
      }, {
        headers: {
            'Authorization': `Bearer ${localStorage.access_token}`
        }
      }).then((res)=>{
        console.log(res);
        toast.success('Password updated! Please login with the new password')
        history.push('/login')
      }).catch((err)=>{
        console.log(err)
        setErrorMessage(err.response.data.message)
        toast.error(err.response.data.message);
    })

  }
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
};
const handleClose = () => {
    setAnchorEl(null);
};

  return (
    <ThemeProvider theme={theme}>
      <AppBar style={{background: '#1976d285'}} position="static">
      <Toolbar style={{display:'flex', justifyContent:'flex-end'}}>
    

<div >
    <Button
        style={{color: 'white'}}
        id="basic-button"
        aria-controls={open ? 'basic-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
    >
        Accounts
    </Button>
    <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
            'aria-labelledby': 'basic-button',
        }}
    >
        <MenuItem onClick={()=>{history.push('/manage_subscriptions')}}>Plans</MenuItem>
        <MenuItem onClick={()=>{history.push('/dashboard')}}>Home</MenuItem>
    </Menu>
</div>
      </Toolbar>
      </AppBar>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockResetIcon />
          </Avatar>
          <Typography component="h1" style={{color:'white'}} variant="h5">
            Enter a new password
          </Typography>
          <Box component="form" noValidate sx={{ mt: 1 }}>
          <TextField
              margin="normal"
              value={password}
              
              onChange={(e) => { setPassword(e.target.value); setErrorMessage('') }}
              required
              fullWidth
              sx={textInputStyle}
              InputLabelProps={{ shrink: true }} 

              name="password"
              placeholder="Password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
             <TextField
              margin="normal"
     
              sx={textInputStyle}
              required
              InputLabelProps={{ shrink: true }} 

              fullWidth
              
              name="password"
              placeholder="Confirm Password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              onClick={updatePasswordHandler}
              sx={{ mt: 3, mb: 2 }}
            >
             Update Password
            </Button>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
}