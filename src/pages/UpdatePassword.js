import LockResetIcon from '@mui/icons-material/LockReset';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import * as React from 'react';
import axios from 'axios';
import { useDispatch } from 'react-redux';
import { useHistory } from "react-router-dom";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const theme = createTheme();
export default function UpdatePassword() {
  const [password, setPassword] = React.useState('');
  let history = useHistory();
  const [errorMessage, setErrorMessage] = React.useState('')
  const dispatch = useDispatch()
  toast.configure();

  const updatePasswordHandler = (e) => {
      e.preventDefault()
      axios.put('https://kai-pengine.herokuapp.com/user/pass', {
          password
      }).then((res)=>{
        console.log('res', res)
        toast.success(res.data.message)
      }).catch((err)=>{
        console.log(err)
        setErrorMessage(err.response.data.message)
        toast.error(err.response.data.message);
    })

  }

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockResetIcon />
          </Avatar>
          <Typography component="h1" style={{color:'white'}} variant="h5">
            Enter a new password
          </Typography>
          <Box component="form" noValidate sx={{ mt: 1 }}>
          <TextField
              margin="normal"
              value={password}
              onChange={(e) => { setPassword(e.target.value); setErrorMessage('') }}
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              onClick={updatePasswordHandler}
              sx={{ mt: 3, mb: 2 }}
            >
             Update Password
            </Button>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
}