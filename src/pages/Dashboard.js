import InputUnstyled from '@mui/base/InputUnstyled';
import { AppBar, Button, Menu, MenuItem, Toolbar } from '@mui/material';
import Box from '@mui/material/Box';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from "react-router-dom";
import { getLocation } from '../api';
import { StyledInputElement } from '../constants';
import { dropoffPincode, pickupPincode } from '../redux/actions/actions';
import '../styles/Dashboard.css';
import intl from '../translations';
import {hotjar} from 'react-hotjar';


function Dashboard() {
  const dispatch = useDispatch()
  let history = useHistory();
  hotjar.initialize(3017166, 6);
  hotjar.identify('USER_ID', {userProperty: 'value'});
  hotjar.event('button-click');
  hotjar.stateChange('/dashboard');
  const [pickup, setPickup] = React.useState('')
  const [dropoff, setDropoff] = React.useState('')
  const [pickupCity, setPickupCity] = React.useState('')
  const [pickupState, setPickupState] = React.useState('')
  const [pickupCountry, setPickupCountry] = React.useState('')
  const [dropoffCity, setDropoffCity] = React.useState('')
  const [dropoffState, setDropoffState] = React.useState('')
  const [dropoffCountry, setDropoffCountry] = React.useState('')
  const [pickUploading, setPickUpLoading] = React.useState(false);
  const appAuth = useSelector(({ auth }) => auth)
  console.log(appAuth?.currentPlan);
  console.log(appAuth?.currentUser);

  React.useEffect(()=>{
    if(appAuth?.currentUser === '' || appAuth?.currentPlan === ''){
      history.push('/login')
    }
  })
  React.useEffect(() => {
    if (pickup.length > 4) {
      setPickUpLoading(true);
      setTimeout(() => {
        getLocation(pickup).then((res) => {
          setPickupCity(res?.data?.location[0].city);
          setPickupState(res?.data?.location[0].state);
          setPickupCountry(res?.data?.location[0].country);
        }).catch((err) => {
        }).finally(() => {
          setPickUpLoading(false);
        })
      }, [2000])

    } else {
      setPickupCity('');
    }
  }, [pickup])
  const [loading, setLoading] = React.useState(false);

  React.useEffect(() => {
    if (dropoff.length > 4) {
      setLoading(true);
      setTimeout(() => {
        getLocation(dropoff).then((res) => {
          setDropoffCity(res?.data?.location[0].city);
          setDropoffState(res?.data?.location[0].state);
          setDropoffCountry(res?.data?.location[0].country);
        }).catch((err) => {
        }).finally(() => {
          setLoading(false);
        })
      }, [2000])
    } else {
      setDropoffCity('');
    }
  }, [dropoff])
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
};
const handleClose = () => {
    setAnchorEl(null);
};
  return (

    <div className='dashboard'>
      
      <AppBar style={{background: '#1976d285'}} position="static">
      <Toolbar style={{display:'flex', justifyContent:'flex-end'}}>
    

<div >
    <Button
        style={{color: 'white'}}
        id="basic-button"
        aria-controls={open ? 'basic-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
    >
        Accounts
    </Button>
    <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
            'aria-labelledby': 'basic-button',
        }}
    >
        <MenuItem onClick={()=>{history.push('/reset_password')}}>Reset Password</MenuItem>
        <MenuItem onClick={()=>{history.push('/manage_subscriptions')}}>Plans</MenuItem>
        <MenuItem onClick={()=>{history.push('/')}}>Logout</MenuItem>
    </Menu>
</div>
      </Toolbar>
      </AppBar>
      <Box
        sx={{
          boxShadow: 3,
          width: '80rem',
          height: '60vh',
          margin: 'auto',
          bgcolor: (theme) => (theme.palette.mode === 'dark' ? '#101010' : '#fff'),
          color: (theme) =>
            theme.palette.mode === 'dark' ? 'grey.300' : 'grey.800',
          p: 1,
          m: 1,
          borderRadius: 2,
          textAlign: 'center',
          position: 'relative',
          fontSize: '0.875rem',
          fontWeight: '700',
        }} >
        <div className="dashboard-container">
          <div className="dashboard-container-label">
            <h2>{intl.formatMessage({
              id: 'app.dashboard.pickup',
            })}</h2>
            <h2>{intl.formatMessage({
              id: 'app.dashboard.dropoff',
            })}</h2>
          </div>
          <div className="dashboard-container-input">
            <div>
              <InputUnstyled value={pickup} onChange={(e) => { setPickup(e.target.value) }} aria-label="Demo input" placeholder="Where is the pickup to be made?" components={{ Input: StyledInputElement }} />
              {pickUploading ? <h3>Loading</h3> : <h3>{pickupCity} {pickupCity?.length > 1 && ', '} {pickupState} {pickupState?.length > 1 && ', '} {pickupCountry}</h3>}
            </div>
            <div>
              <InputUnstyled value={dropoff} onChange={(e) => { setDropoff(e.target.value) }} aria-label="Demo input" placeholder="Where is the delivery to be made?" components={{ Input: StyledInputElement }} />
              {loading ? <h3>Loading</h3> : <h3>{dropoffCity} {dropoffCity?.length > 1 && ', '} {dropoffState} {dropoffState?.length > 1 && ', '} {dropoffCountry}</h3>}
            </div>
          </div>

        </div>
        <div className="dashboard-next-button"> <Button disabled={pickup === '' | dropoff === ''} onClick={() => {
          dispatch(pickupPincode(pickup));
          dispatch(dropoffPincode(dropoff));
          history.push('/dashboard/1316cc17-82f6-4d2')
        }}>{intl.formatMessage({
          id: 'app.dashboard.next',
        })}</Button></div>
      </Box>

    </div>
  )
}

export default Dashboard