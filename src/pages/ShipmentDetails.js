import InputUnstyled from '@mui/base/InputUnstyled';
import Button from '@mui/material/Button';
import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from "react-router-dom";
import ShipmentOptions from '../components/ShipmentOptions';
import TextInputFields from '../components/TextInputFields';
import { StyledInputElement } from '../constants';
import { shipmentDetails, totalLoadingMeter } from '../redux/actions/actions';
import '../styles/ShipmentDetails.css';
import intl from '../translations';
import AddIcon from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';
import { Typography } from '@mui/material';

function ShipmentDetails() {
  const [rows, setRows] = React.useState(1);
  const [selectedOption, setSelectedOption] = React.useState('Euro Pallet');
  const [other, setOtherData] = React.useState([
    { id: 1 }
  ])
  React.useEffect(() => {
    setOtherData(other)
  }, [other])
  const dispatch = useDispatch()
  const [number, setNumber] = React.useState('')
  const [length, setLength] = React.useState('');
  let history = useHistory();
  const [width, setWidth] = React.useState('');
  const [height, setHeight] = React.useState('');
  const EURO_PALLET_LENGTH = 80;
  const EURO_PALLET_WIDTH = 120;
  const EURO_PALLET_HEIGHT = 120;
  const [numberOfPallets, setNumberOfPallets] = React.useState(0);
  const [totalPalletWeight, setTotalPalletWeight] = React.useState(0);
  const [weight, setWeight] = React.useState('');
  
  const submitHandler = () => {
    // calculate the correct data before dispatching


      if (selectedOption !== 'Euro Pallet') {
        let inputArray = inputList;
        console.log(inputList);
        let totalLength = inputArray.map(item => item.length).reduce((prev, next) => parseFloat(prev) + parseFloat(next));
        let totalWidth = inputArray.map(item => item.width).reduce((prev, next) => parseFloat(prev) + parseFloat(next));
        let totalNumber = inputArray.map(item => item.number).reduce((prev, next) => parseFloat(prev) + parseFloat(next));
        let totalHeight = inputArray.map(item => item.height).reduce((prev, next) => parseFloat(prev) + parseFloat(next));
        let totalWeight = inputArray.map(item => item.weight).reduce((prev, next) => parseFloat(prev) + parseFloat(next));
        console.log('total loading meter', loadingMeter);    
        dispatch(totalLoadingMeter(loadingMeter))
        dispatch(shipmentDetails({
          number:totalNumber,
          length:totalLength,
          width:totalWidth,
          height:totalHeight,
          weight:totalWeight
        }))
      } else {
        dispatch(totalLoadingMeter((numberOfPallets*EURO_PALLET_LENGTH*EURO_PALLET_WIDTH)/24000))
        dispatch(shipmentDetails({
          number: numberOfPallets,
          length: EURO_PALLET_LENGTH,
          width: EURO_PALLET_WIDTH,
          height: EURO_PALLET_HEIGHT,
          weight: totalPalletWeight
        }))
      }
      history.push('/dashboard/1316cc17-82f6-4d2/price')
  }
  const [inputList, setInputList] = React.useState([])
  const [loadingMeter, setLoadingMeter] = React.useState(0)

  const sendValuesBack = (e) => {
    console.log('values:', e);
    setInputList([...inputList, e]);
  }

  const sendLoadingMeter = (e) => {
    setLoadingMeter(loadingMeter+parseFloat(e));
  }

  React.useEffect(()=>{
    console.log(inputList);
  })
  return (
    <div className="shipmentDetails-container">
      <div className="shipmentDetails-shipment-details">
        <div className="shipmentDetails-shipment-details-header">
        <Typography marginBottom={2} color="white" variant="h6">
        {intl.formatMessage({
            id: 'app.shipment.enterShipmentDetails',
          })}
        </Typography>
        </div>
        <div className="shipmentDetails-shipment-details-content">
        <Typography marginBottom={2} color="white" variant="h6">
        {intl.formatMessage({
            id: 'app.shipment.typeOfPackaging',
          })}
                              </Typography>
        </div>
          <ShipmentOptions selectedOption={selectedOption} setSelectedOption={setSelectedOption} />
      </div>
      {selectedOption === 'Euro Pallet' && (
        <div className="shipmentDetails-shipment-fields">
          <div className="shipmentDetails-shipment-fields-header">
            <h4>{intl.formatMessage({
              id: 'app.shipment.numberOfPallets',
            })}</h4>
            <div> <InputUnstyled value={numberOfPallets} onChange={(e) => { setNumberOfPallets(e.target.value) }} aria-label="Demo input" placeholder="Enter the number of pallets" components={{ Input: StyledInputElement }} /></div>
          </div>
          <div className="shipmentDetails-shipment-fields-header">
            <h4>{intl.formatMessage({
              id: 'app.shipment.totalWeight',
            })}</h4>
            <div> <InputUnstyled value={totalPalletWeight} onChange={(e) => { setTotalPalletWeight(e.target.value) }} aria-label="Demo input" placeholder="Enter the total weight" components={{ Input: StyledInputElement }} /></div>
          </div>
        </div>
      )}
      {((selectedOption === 'Pallet') || (selectedOption === 'Box') || (selectedOption === 'Crate') || (selectedOption === 'Bundle') || (selectedOption === 'Drum') || (selectedOption === 'Roll') || (selectedOption === 'Bale')) && (
        <div>
          <div className="shipmentDetails-shipment-fields-others">
           {Array.from(Array(rows).keys()).map((item, index)=> (
                <TextInputFields number={number} length={length} width={width} height={height} weight={weight} setLength={setLength}
                setWidth={setWidth} setHeight={setHeight} setNumber={setNumber} setWeight={setWeight} sendLoadingMeter={sendLoadingMeter} sendValuesBack={sendValuesBack} />
           ))}
          </div>
          <div className="icons-wrapper">
         <Button onClick={()=>{
           setRows(rows+1);
         }}> <AddIcon/></Button>
         <div> <Button onClick={()=>{
           if(rows !== 1){
            setRows(rows-1);
            let a = inputList;
            a.pop();
            setInputList(a)
           }

         }}><CloseIcon /></Button></div>
        </div>
        </div>
      )}
      <div className="dashboard-shipment-button">
        <Button className={selectedOption !== 'Euro Pallet' ? `next-button` : `next-button-euro-pallet`} onClick={submitHandler}>
          {intl.formatMessage({
            id: 'app.shipment.next',
          })}
        </Button></div>
    </div>
  )
}

export default ShipmentDetails