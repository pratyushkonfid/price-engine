import LockResetIcon from '@mui/icons-material/LockReset';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import axios from 'axios';
import * as React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from "react-router-dom";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Navbar from '../components/Navbar';
import { textInputStyle } from '../constants';

const theme = createTheme();
export const validateEmail = (mail) =>
{
 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
  {
    return (true)
  }
    return (false)
}
export default function ForgotPassword() {
  const [email, setEmail] = React.useState('')
  const [emailSentDiv, setEmailSentDiv] = React.useState(false);
  const [password, setPassword] = React.useState('');
  let history = useHistory();
  const [errorMessage, setErrorMessage] = React.useState('')
  const dispatch = useDispatch()
  toast.configure();

  const resetHandler = (e) => {
    e.preventDefault()
    axios.post('https://kaizentestapi.herokuapp.com/verify/',  {
      email: email,
      reason: "forget_password"
    }).then((res)=>{
        console.log(res)
         setEmailSentDiv(true);
    }).catch((err)=>{
        console.log(err);
    })
  }

  return (
    <ThemeProvider theme={theme}>
            <Navbar />
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockResetIcon />
          </Avatar>
          <Typography component="h1" style={{color:'white'}} variant="h5">
            Forgot Password
          </Typography>
         { !emailSentDiv ? <>  <Box component="form" noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              value={email}
              onChange={(e) => { setEmail(e.target.value); setErrorMessage('') }}
              required
              fullWidth
              sx={textInputStyle}
              id="email"
              placeholder="Enter your registered email"
              name="email"
              autoComplete="email"
              autoFocus
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              onClick={resetHandler}
              sx={{ mt: 3, mb: 2 }}
            >
             Send Email
            </Button>
            <Grid container>
              <Grid item>
                <Link href="/signup" variant="body2" style={{ color: 'white' }}>
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </Box></> : <><Box>    <Typography component="h2" style={{color:'white', marginTop:'20px'}} variant="h6">
            Email with a confirmation link sent!
          </Typography></Box></>
           }
        </Box>
      </Container>
    </ThemeProvider>
  );
}