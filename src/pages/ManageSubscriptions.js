import { AppBar, Button, Menu, MenuItem, Toolbar } from '@mui/material';
import axios from 'axios';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import DiamondSubscription from '../components/DiamondSubscription';
import EnterpriseSubscription from '../components/EnterpriseSubscription';
import FreeSubscription from '../components/FreeSubscription';
import GoldSubscription from '../components/GoldSubscription';
import { diamondProduct, goldProduct } from '../constants';
import { checkoutPayment } from '../redux/actions/actions';
import '../styles/ManageSubscriptions.css';


export const ManageSubscriptions = () => {
    toast.configure();
    const dispatch = useDispatch()
    const appAuth = useSelector(({ auth }) => auth)
    let history = useHistory();

    console.log('data', appAuth);

    React.useEffect(()=>{
        if(appAuth?.currentUser === '' || appAuth?.currentPlan === ''){
          history.push('/login')
        }
      })
    async function goldTokenHandler(token) {
        // const response = await dispatch(checkoutPayment(token, goldProduct))
        axios.post('https://price-test-engine.herokuapp.com/checkout', { token, goldProduct }).then((res)=>{
            console.log(res)
            toast.success("Success! Check email for details")
        }).catch((err)=>{
            alert("Something went wrong", { type: "error" });
            toast.error(err.message);
        })
        // const { status } = response.data;
    }
    async function diamondTokenHandler(token) {
        const response = await dispatch(checkoutPayment(token, diamondProduct))
        const { status } = response.data;
        if (status === "success") {
            toast.success("Success! Check email for details")
        } else {
            alert("Something went wrong", { type: "error" });
            toast.error("Something went wrong");
        }
    }

    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
      setAnchorEl(null);
  };

    return (
        <div className="manage-subscriptions">
             <AppBar style={{background: '#1976d285'}} position="static">
             <Toolbar style={{display:'flex', justifyContent:'flex-end'}}>
    

<div >
    <Button
        style={{color: 'white'}}
        id="basic-button"
        aria-controls={open ? 'basic-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
    >
        Accounts
    </Button>
    <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
            'aria-labelledby': 'basic-button',
        }}
    >
        <MenuItem onClick={()=>{history.push('/reset_password')}}>Reset Password</MenuItem>
        <MenuItem onClick={()=>{history.push('/dashboard')}}>Home</MenuItem>
    </Menu>
</div>
      </Toolbar>
      </AppBar>
            <div className="manage-subscriptions-logo"> </div>
            <div className="manage-subscriptions-cards">
                <FreeSubscription />
                <GoldSubscription />
                <DiamondSubscription diamondTokenHandler={diamondTokenHandler} />
                <EnterpriseSubscription />

            </div>
        </div>
    )
}

export default ManageSubscriptions