import React from 'react'
import './Contact.css'
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import FormControlLabel from '@mui/material/FormControlLabel';
import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import { StyledInputElement, textInputStyle } from '../constants';
import Navbar from '../components/Navbar'
function ContactPage() {
  const [email, setEmail] = React.useState('')
  const [password, setPassword] = React.useState('');
  const theme = createTheme();
  return (
    <div className="contactpage">
      <Navbar />
        <div class="header">Contact Us</div>
        <div class="body"> <a style={{textDecoration: 'none'}} href="mailto:sales@kaizentech.io">Email : sales@kaizentech.io</a></div>
        <div>
        <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Box component="form"  noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              onChange={(e) => { setEmail(e.target.value);  }}
              required
              style={{color:'white'}}
              fullWidth
              type="text"
              InputLabelProps={{ shrink: true }} 

              sx={textInputStyle}
              id="text"
              placeholder="Enter your name"
              name="text"
              
              autoFocus
            />
             <TextField
              margin="normal"
              // value={email}
              onChange={(e) => { setEmail(e.target.value) }}
              required
              sx={textInputStyle}
              fullWidth
              id="email"
              placeholder="Email Address"
              InputLabelProps={{ shrink: true }} 
              name="email"
              autoComplete="email"
              autoFocus
            />
          <TextField
              margin="normal"
              onChange={(e) => { setEmail(e.target.value);  }}
              required
              style={{color:'white'}}
              fullWidth
              type="text"
              InputLabelProps={{ shrink: true }} 
              id="text"
              placeholder="Enter your role"
              sx={textInputStyle}
              name="text"
              
              autoFocus
            />
               <TextField
              margin="normal"
              onChange={(e) => { setEmail(e.target.value);  }}
              required
              style={{color:'white'}}
              InputLabelProps={{ shrink: true }} 
              fullWidth
              sx={textInputStyle}
              type="text"
              id="text"
              placeholder="Body"
              name="text"
              
              autoFocus
            />
          
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Contact
            </Button>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
        </div>
    </div>
  )
}

export default ContactPage