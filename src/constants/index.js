import { styled } from '@mui/system';
export const blue = {
  100: '#DAECFF',
  200: '#80BFFF',
  400: '#3399FF',
  600: '#0072E5',
};

export const grey = {
  50: '#F3F6F9',
  100: '#E7EBF0',
  200: '#E0E3E7',
  300: '#CDD2D7',
  400: '#B2BAC2',
  500: '#A0AAB4',
  600: '#6F7E8C',
  700: '#3E5060',
  800: '#2D3843',
  900: '#1A2027',
};

export const goldProduct = {
  name: 'Price Engine Gold Subscription',
  price: 1499,
}

export const diamondProduct = {
  name: 'Price Engine Diamond Subscription',
  price: 2499
}

export const StyledInputElement = styled('input')(
  ({ theme }) => `
  width: 320px;
  font-size: 0.875rem;
  font-family: IBM Plex Sans, sans-serif;
  font-weight: 400;
  line-height: 1.5;

  border-radius: 8px;
  padding: 12px 12px;

`,
);

export const textInputStyle = { input: { color: 'white', background: 'transparent', border:'1px solid white' }, label: {color: "white"}, }